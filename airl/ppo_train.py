from tqdm import tqdm
from ppo import *
import torch
#from gym_wrapper import *
import wandb
import argparse
import os
import pickle
from matplotlib import pyplot as plt
import gym_examples
import gym
import sys
print(sys.executable)

"""
This code is a modification



"""

# Use GPU if available
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

if __name__ == '__main__':

    # Init WandB & Parameters
    
    wandb.init(project='PPO', config={
        'env_id': 'small_museum_collect_v0',
        'env_steps': 16000000,
        'batchsize_ppo': 32,
        'n_workers': 8,
        'lr_ppo': 3e-4,
        'entropy_reg': 0.03,
        'lambd': [1, 1, 1, 1],
        'gamma': 0.999,
        'epsilon': 0.1,
        'ppo_epochs': 5
    })
    config = wandb.config
    
    # Create Environments
    #env_list  = [gym.make("gym_examples/SimpleGrid-v0") for i in range(config.n_workers)]
    #env_list  = [gym.make("gym_examples/AIRLDemo-v0") for i in range(config.n_workers)]
    env_list  = [gym.make("gym_examples/SmallMuseumCollect-v0") for i in range(config.n_workers)]

    # Generate random grids and get states
    states = []
    for i in range(config.n_workers):
        states.append(env_list[i].reset())

    states = np.stack(states, axis=0)
    states_tensor = torch.tensor(states).float().to(device)

    # Fetch shapes
    n_actions = env_list[0].n_actions
    obs_shape = env_list[0].observation_space.shape
    state_shape = obs_shape[1:]
    in_channels = obs_shape[0]
    #state_shape = obs_shape[:-1]
    #in_channels = obs_shape[-1]

    # Initialize Models
    ppo = PPO(state_shape=state_shape, in_channels=in_channels, n_actions=n_actions).to(device)
    optimizer = torch.optim.Adam(ppo.parameters(), lr=config.lr_ppo)
    dataset = TrajectoryDataset(batch_size=config.batchsize_ppo, n_workers=config.n_workers)

    for t in tqdm(range(int(config.env_steps / config.n_workers))):

        # Sampling ----------------------------------------------------------------------------------
        actions, log_probs = ppo.act(states_tensor)

        next_states = []
        rewards = []
        dones = []
        for i in range(len(env_list)):
            # Compute reward of current state
            ith_next_states, ith_rewards, ith_done, _  = env_list[i].step(actions[i]) # the done is w.r.t state not next_state (fix it?)
            rewards.append(ith_rewards)
            next_states.append(ith_next_states)
            dones.append(ith_done)

            #if ith_done:
                #env_list[i].update_state(env_list[i].reset())
                #next_states[i] = env_list[i].state.copy()

        # Data treatment
        next_states = np.stack(next_states, axis=0) # correct format
        scalarized_rewards = [sum([config.lambd[i] * r[i] for i in range(len(r))]) for r in rewards]

        # Save trajectories for ppo to sample
        train_ready = dataset.write_tuple(states, actions, scalarized_rewards, dones, log_probs, rewards)

        # PPO ---------------------------------------------------------------------------------
        # This runs only when enough demonstrations are gathered (see write_tuple method)
        if train_ready:
            update_policy(ppo, dataset, optimizer, config.gamma, config.epsilon, config.ppo_epochs,
                          entropy_reg=config.entropy_reg)
            objective_logs = dataset.log_objectives()
            #for i in range(objective_logs.shape[1]):
                #wandb.log({'Obj_' + str(i): objective_logs[:, i].mean()})
            wandb.log({'Movement penalties': objective_logs[:, 0].mean()})
            wandb.log({'Target A collection': objective_logs[:, 1].mean()})
            wandb.log({'Target B collection': objective_logs[:, 2].mean()})
            wandb.log({'Collision penalties': objective_logs[:, 3].mean()})

            for ret in dataset.log_returns():
                wandb.log({'Returns': ret})
            dataset.reset_trajectories()
        # End PPO ---------------------------------------------------------------------------------
        
        # This is a continuation of the sampling part
        # Prepare state input for next time step
        if not train_ready:
            states = next_states.copy()
            states_tensor = torch.tensor(states).float().to(device)
            #for i in range(len(env_list)): 
                #env_list[i].update_state(states[i]) # Also updates states in environment



    #cwd = os.getcwd()
    #torch.save(ppo.state_dict(), cwd + '/teste' + '.pt')
