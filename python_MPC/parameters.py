import numpy as np

def get_parameters(env_type):


    if env_type == 'old':

        # Set initial targets, initial and temrinal conditions
        # ATTENTION: MAKE SURE TO DECLARE TARGETS IN THE SAME ORDER AS IN MATLAB IN 'motion_planner.py' line 51!!!!!!!!
        tar_list_A = []
        tar_list_A.append((6,4))                                
        tar_list_A.append((5,6))


        tar_list_B = []
        tar_list_B.append((0,3))
        tar_list_B.append((4,1))

        ini_pos = [(1,1)]

        n_tar = len(tar_list_A) + len(tar_list_B)

        terminal = [(1,5)]

        # Initialize multi-robot system
        x_0 = np.matrix([
            [-3.0,-2.0],
            [0.0,0.0],
            [3.0,3.0],
            [0.0,0.0]
        ])


        env_params = {
            'tar_list_A': tar_list_A,
            'tar_list_B': tar_list_B,
            'ini_pos': ini_pos,
            'terminal_state': terminal,
            'human_length': 0.290,
            'human_width': 0.410,
            'human_security_margin': 0.100, # not used but dont forget
            'env_width': 7.0,
            'env_length': 7.0
                    }

        mrs_params = {
            'number_of_robots': 2,
            'dynamics': "double_integrator",
            'sampling_period': 1.0,
            'initial_state': x_0,
            'robot_length': 0.5,
            'robot_width': 0.5
                }

        matlab_params = {
                    'N': 25, # CAN POTENTIALLY BE REDUCED FOR FASTER SOLVE TIME
                    'nx': 4,
                    'nu': 2,
                    'n_agents': 3
                }
        
    elif env_type == 'collision_scenario_collab':
        # Set initial targets, initial and temrinal conditions
        # ATTENTION: MAKE SURE TO DECLARE TARGETS IN THE SAME ORDER AS IN MATLAB IN 'motion_planner.py' line 51!!!!!!!!
        tar_list_A = []
        tar_list_A.append((6,4))                                
        tar_list_A.append((5,6))


        tar_list_B = []
        tar_list_B.append((0,3))
        tar_list_B.append((2,4))

        ini_pos = [(2,0)]

        n_tar = len(tar_list_A) + len(tar_list_B)

        terminal = [(1,5)]

        # Initialize multi-robot system
        x_0 = np.matrix([
            [-3.0,-2.0],
            [0.0,0.0],
            [3.0,3.0],
            [0.0,0.0]
        ])


        env_params = {
            'tar_list_A': tar_list_A,
            'tar_list_B': tar_list_B,
            'ini_pos': ini_pos,
            'terminal_state': terminal,
            'human_length': 0.290,
            'human_width': 0.410,
            'human_security_margin': 3.0, # side of the safety region square
            'env_width': 7.0,
            'env_length': 7.0
                    }

        mrs_params = {
            'number_of_robots': 2,
            'dynamics': "double_integrator",
            'sampling_period': 1.0,
            'initial_state': x_0,
            'robot_length': 0.5,
            'robot_width': 0.5
                }

        matlab_params = {
                    'N': 25, # CAN POTENTIALLY BE REDUCED FOR FASTER SOLVE TIME
                    'nx': 4,
                    'nu': 2,
                    'ny': 2,
                    'n_agents': 3,
                    'n_obstacles': 4,
                    'n_obs_sides': 4
                }

    # ENVIRONMENT 1
    elif env_type == 'environment1':
        # Set initial targets, initial and temrinal conditions
        # ATTENTION: MAKE SURE TO DECLARE TARGETS IN THE SAME ORDER AS IN MATLAB IN 'motion_planner.py' line 51!!!!!!!!
        tar_list_A = []
        tar_list_A.append((6,4))                                
        tar_list_A.append((5,6))


        tar_list_B = []
        tar_list_B.append((4,3))
        tar_list_B.append((0,5))

        ini_pos = [(2,0)]

        n_tar = len(tar_list_A) + len(tar_list_B)

        terminal = [(1,5)]

        # Initialize multi-robot system
        x_0 = np.matrix([
            [-3.0,-2.0],
            [0.0,0.0],
            [3.0,3.0],
            [0.0,0.0]
        ])


        env_params = {
            'tar_list_A': tar_list_A,
            'tar_list_B': tar_list_B,
            'ini_pos': ini_pos,
            'terminal_state': terminal,
            'human_length': 0.290,
            'human_width': 0.410,
            'human_security_margin': 3.0, # side of the safety region square
            'env_width': 7.0,
            'env_length': 7.0
                    }

        mrs_params = {
            'number_of_robots': 2,
            'dynamics': "double_integrator",
            'sampling_period': 1.0,
            'initial_state': x_0,
            'robot_length': 0.25,
            'robot_width': 0.25
                }

        matlab_params = {
                    'N': 30, # CAN POTENTIALLY BE REDUCED FOR FASTER SOLVE TIME
                    'nx': 4,
                    'nu': 2,
                    'ny': 2,
                    'n_agents': 3,
                    'n_obstacles': 4,
                    'n_obs_sides': 4
                }
        
    # ENVIRONMENT 2 
    elif env_type == 'environment2':
        # Set initial targets, initial and temrinal conditions
        # ATTENTION: MAKE SURE TO DECLARE TARGETS IN THE SAME ORDER AS IN MATLAB IN 'motion_planner.py' line 51!!!!!!!!
        tar_list_A = [] # GREEN
        tar_list_A.append((1,1))                                
        tar_list_A.append((3,0))


        tar_list_B = [] # BLUE
        tar_list_B.append((4,1))
        tar_list_B.append((2,2))
        #tar_list_B.append((5,5))
        ini_pos = [(4,4)]# human

        n_tar = len(tar_list_A) + len(tar_list_B)

        terminal = [(1,5)]

        # Initialize multi-robot system (continuous values!!!)
        x_0 = np.matrix([ 
            [1.0,3.0],
            [0.0,0.0],
            [-3.0,-3.0],
            [0.0,0.0]
        ])


        env_params = {
            'tar_list_A': tar_list_A,
            'tar_list_B': tar_list_B,
            'ini_pos': ini_pos,
            'terminal_state': terminal,
            'human_length': 0.290,
            'human_width': 0.410,
            'human_security_margin': 3.0, # side of the safety region square
            'env_width': 7.0,
            'env_length': 7.0
                    }

        mrs_params = {
            'number_of_robots': 2,
            'dynamics': "double_integrator",
            'sampling_period': 1.0,
            'initial_state': x_0,
            'robot_length': 0.25,
            'robot_width': 0.25
                }

        matlab_params = {
                    'N': 30, # CAN POTENTIALLY BE REDUCED FOR FASTER SOLVE TIME
                    'nx': 4,
                    'nu': 2,
                    'ny': 2,
                    'n_agents': 3,
                    'n_obstacles': 4,
                    'n_obs_sides': 4
                }

    return env_params, mrs_params, matlab_params