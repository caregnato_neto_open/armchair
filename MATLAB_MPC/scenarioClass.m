classdef scenarioClass < handle
    
    
    properties
        max_mission_time = [];
        number_obs = [];
        number_tar = [];
        number_agents = [];
        base = [];
        grid_size_x = [];
        grid_size_y = [];
        cell_size_x = [];
        cell_size_y = [];
        op_region_size_x = [];
        op_region_size_y = [];
        n_agents = [];
        op_region_center = [];
        obs = [];
        tar = [];
        ini_state = [];
    end
    
    methods
        
        function obj = scenarioClass(scenario)
            obj.number_obs = scenario.number_obs;
            obj.number_tar = scenario.number_tar;
            obj.number_agents = scenario.n_agents;
            obj.grid_size_x = scenario.grid_size_x;
            obj.grid_size_y = scenario.grid_size_y;
            obj.cell_size_x = scenario.cell_size_x;
            obj.cell_size_y = scenario.cell_size_y;
            obj.op_region_size_x = scenario.op_region_size_x;
            obj.op_region_size_y = scenario.op_region_size_y;
            obj.op_region_center = scenario.op_region_center;
         %   if empt
            obj.obs = scenario.obs;
            obj.tar = scenario.tar;
            obj.ini_state = scenario.ini_state;
        end
        
        function BuildEnvironment(obj,type)
            
            switch type
                
                
                %% Test scenario
                case "test"
                    % Operation region
                    op_region_width = 4;
                    op_region_length = 4;
                    op_region_center = [0,0];
                    
                    obj.polytopes.op_region.P = [eye(2);-eye(2)];
                    obj.polytopes.op_region.q = [op_region_center(1) + op_region_length/2;
                        op_region_center(2) + op_region_width/2;
                        op_region_center(1) + op_region_length/2;
                        op_region_center(2) + op_region_width/2];
                    
                    
                    obs1_width = 0.3;
                    obs1_length = 2;
                    obs1_center = [-1,0];
                    
                    obj.polytopes.obs{1}.P = [eye(2);-eye(2)];
                    obj.polytopes.obs{1}.q = [obs1_center(1) + obs1_length/2;
                        obs1_center(2) + obs1_width/2;
                        -(obs1_center(1) - obs1_length/2);
                        -(obs1_center(2) - obs1_width/2)];
                    
                    obj.number_obs = length(obj.polytopes.obs);
                    
                    
                    %%  Sewer pipes
                case "vss_sewer_pipes"
                    op_region_width = 1.3;
                    op_region_length = 1.5;
                    op_region_center = [0,0];
                    
                    % Operation region
                    obj.polytopes.op_region.P = [eye(2);-eye(2)];
                    obj.polytopes.op_region.q = [op_region_center(1) + op_region_length/2;
                        op_region_center(2) + op_region_width/2;
                        op_region_center(1) + op_region_length/2;
                        op_region_center(2) + op_region_width/2];
                    
                    
                    % Obstacles
                    
                    obs_center{1} = [-0.375 0.325];
                    obs_dimensions{1} = [0.5 0.5]; % width x length
                    
                    obs_center{2} = [-0.375+(op_region_length/2) 0.325];
                    obs_dimensions{2} = [0.5 0.5]; % width x length
                    
                    obs_center{3} = [-0.375 0.325-(op_region_width/2)];
                    obs_dimensions{3} = [0.5 0.5]; % width x length
                    
                    obs_center{4} = [-0.375+(op_region_length/2) 0.325-(op_region_width/2)];
                    obs_dimensions{4} = [0.5 0.5]; % width x length
                    
                    
                    obj.number_obs = length(obs_center);
                    
                    for i = 1 : obj.number_obs
                        obj.polytopes.obs{i}.P = [eye(2);-eye(2)];
                        obj.polytopes.obs{i}.q = [obs_center{i}(1) + obs_dimensions{1}(1)/2;
                            obs_center{i}(2) + obs_dimensions{i}(2)/2;
                            -(obs_center{i}(1) - obs_dimensions{i}(1)/2);
                            -(obs_center{i}(2) - obs_dimensions{i}(2)/2)];
                    end
                    
                    %% VLC sewer pipes
                case "VLC_sewer_pipes"
                    op_region_width = 6;
                    op_region_length = 6;
                    op_region_center = [0,0];
                    
                    % Operation region
                    obj.polytopes.op_region.P = [eye(2);-eye(2)];
                    obj.polytopes.op_region.q = [op_region_center(1) + op_region_length/2;
                        op_region_center(2) + op_region_width/2;
                        op_region_center(1) + op_region_length/2;
                        op_region_center(2) + op_region_width/2];
                    
                    
                    % Obstacles
                    obs_center{1} = [-(op_region_length/4) (op_region_width/4)];
                    obs_dimensions{1} = 2*[0.5 0.5]; % width x length
                    
                    obs_center{2} = [(op_region_length/4) (op_region_width/4)];
                    obs_dimensions{2} = 2*[0.5 0.5]; % width x length
                    %
                    obs_center{3} = [-(op_region_length/4) -(op_region_width/4)];
                    obs_dimensions{3} = 2*[0.5 0.5]; % width x length
                    %
                    obs_center{4} = [(op_region_length/4) -(op_region_width/4)];
                    obs_dimensions{4} = 2*[0.5 0.5]; % width x length
                    
                    
                    obj.number_obs = length(obs_center);
                    
                    for i = 1 : obj.number_obs
                        obj.polytopes.obs{i}.P = [eye(2);-eye(2)];
                        obj.polytopes.obs{i}.q = [obs_center{i}(1) + obs_dimensions{1}(1)/2;
                            obs_center{i}(2) + obs_dimensions{i}(2)/2;
                            -(obs_center{i}(1) - obs_dimensions{i}(1)/2);
                            -(obs_center{i}(2) - obs_dimensions{i}(2)/2)];
                    end
                    
                case "VLC_sewer_pipes_B"
                    op_region_width = 4;
                    op_region_length = 4;
                    op_region_center = [0,0];
                    
                    % Operation region
                    obj.polytopes.op_region.P = [eye(2);-eye(2)];
                    obj.polytopes.op_region.q = [op_region_center(1) + op_region_length/2;
                        op_region_center(2) + op_region_width/2;
                        op_region_center(1) + op_region_length/2;
                        op_region_center(2) + op_region_width/2];
                    
                    
                    % Obstacles
                    obs_center{1} = [1 -1];
                    obs_dimensions{1} = [4+0.05 0.25]; % width x length
                    %
                    obs_center{2} = [-1 1];
                    obs_dimensions{2} = [4+0.05 0.25]; % width x length
                    %
                    %                     obs_center{3} = [-(op_region_length/4) -(op_region_width/4)];
                    %                     obs_dimensions{3} = 2*[0.5 0.5]; % width x length
                    % %
                    %                     obs_center{4} = [(op_region_length/4) -(op_region_width/4)];
                    %                     obs_dimensions{4} = 2*[0.5 0.5]; % width x length
                    
                    
                    obj.number_obs = length(obs_center);
                    
                    for i = 1 : obj.number_obs
                        obj.polytopes.obs{i}.P = [eye(2);-eye(2)];
                        obj.polytopes.obs{i}.q = [obs_center{i}(1) + obs_dimensions{1}(1)/2;
                            obs_center{i}(2) + obs_dimensions{i}(2)/2;
                            -(obs_center{i}(1) - obs_dimensions{i}(1)/2);
                            -(obs_center{i}(2) - obs_dimensions{i}(2)/2)];
                    end
                    
                case "VLC_sewer_pipes_B_VSS"
                    op_region_width = 1.3;
                    op_region_length = 1.5;
                    op_region_center = [0,0];
                    
                    % Operation region
                    obj.polytopes.op_region.P = [eye(2);-eye(2)];
                    obj.polytopes.op_region.q = [op_region_center(1) + op_region_length/2;
                        op_region_center(2) + op_region_width/2;
                        op_region_center(1) + op_region_length/2;
                        op_region_center(2) + op_region_width/2];
                    
                    % Obstacles
                    obs_center{1} = [-0.4,0.30];
                    obs_dimensions{1} = [0.03,0.7+0.005]; % width x length
                    %
                    obs_center{2} = [0.4,-0.30];
                    obs_dimensions{2} = [0.03,0.7+0.005]; % width x length
                    
                    %                      obs_center{3} = [0.3,-0.45];
                    %                      obs_dimensions{3} = [0.03,0.4+0.005]; % width x length
                    %
                    %                     obs_center{3} = [-(op_region_length/4) -(op_region_width/4)];
                    %                     obs_dimensions{3} = 2*[0.5 0.5]; % width x length
                    % %
                    %                     obs_center{4} = [(op_region_length/4) -(op_region_width/4)];
                    %                     obs_dimensions{4} = 2*[0.5 0.5]; % width x length
                    
                    
                    obj.number_obs = length(obs_center);
                    
                    for i = 1 : obj.number_obs
                        obj.polytopes.obs{i}.P = [eye(2);-eye(2)];
                        obj.polytopes.obs{i}.q = [obs_center{i}(1) + obs_dimensions{1}(1)/2;
                            obs_center{i}(2) + obs_dimensions{i}(2)/2;
                            -(obs_center{i}(1) - obs_dimensions{i}(1)/2);
                            -(obs_center{i}(2) - obs_dimensions{i}(2)/2)];
                    end
                    
            end
            
        end
        
        function BuildMission(obj,type)
            
            switch type
                
                %% test
                case "test"
                    % Max mission time (seconds)
                    obj.max_mission_time = 10;
                    
                    % Targets
                    tar1_width = 0.4;
                    tar1_length = 0.4;
                    tar1_center = [-0.5,1];
                    
                    obj.polytopes.tar{1}.P = [eye(2);-eye(2)];
                    obj.polytopes.tar{1}.q = [tar1_center(1) + tar1_length/2;
                        tar1_center(2) + tar1_width/2;
                        -(tar1_center(1) - tar1_length/2);
                        -(tar1_center(2) - tar1_width/2)];
                    
                    obj.number_tar = length(obj.polytopes.tar);
                    
                    
                    %% sewer pipes
                case "vss_sewer_pipes"
                    % Max mission time (seconds)
                    obj.max_mission_time = 7;
                    
                    % Targets
                    tar_center{1} = [0.7 -0.2];
                    tar_dimensions{1} = [0.1 0.1]; % width x length
                    %
                    %                    tar_center{2} = [0.0 0.1];
                    %                   tar_dimensions{2} = [0.1 0.1]; % width x length
                    %
                    %                     tar_center{3} = [0.0 -0.4];
                    %                     tar_dimensions{3} = [0.1 0.1]; % width x length
                    
                    obj.number_tar = length(tar_center);
                    
                    for i = 1 : obj.number_tar
                        obj.polytopes.tar{i}.P = [eye(2);-eye(2)];
                        obj.polytopes.tar{i}.q = [tar_center{i}(1) + tar_dimensions{1}(1)/2;
                            tar_center{i}(2) + tar_dimensions{i}(2)/2;
                            -(tar_center{i}(1) - tar_dimensions{i}(1)/2);
                            -(tar_center{i}(2) - tar_dimensions{i}(2)/2)];
                    end
                    
                case "VLC_sewer_pipes"
                    
                    obj.max_mission_time = 7;
                    
                    % Targets
                    tar_center{1} = [2.5 -2.5];
                    tar_dimensions{1} = 5*[0.1 0.1]; % width x length
                    %
                    %                                       tar_center{2} = [-2.5 -2.5];
                    %                                      tar_dimensions{2} = 5*[0.1 0.1]; % width x length
                    %
                    %                     tar_center{3} = [0.0 -0.4];
                    %                     tar_dimensions{3} = [0.1 0.1]; % width x length
                    
                    obj.number_tar = length(tar_center);
                    
                    for i = 1 : obj.number_tar
                        obj.polytopes.tar{i}.P = [eye(2);-eye(2)];
                        obj.polytopes.tar{i}.q = [tar_center{i}(1) + tar_dimensions{1}(1)/2;
                            tar_center{i}(2) + tar_dimensions{i}(2)/2;
                            -(tar_center{i}(1) - tar_dimensions{i}(1)/2);
                            -(tar_center{i}(2) - tar_dimensions{i}(2)/2)];
                    end
                    
                    
                case "VLC_sewer_pipes_B"
                    
                    obj.max_mission_time = 7;
                    
                    % Targets
                    tar_center{1} = [-0.5 -1.5];
                    tar_dimensions{1} = 5*[0.1 0.1]; % width x length
                    %
                    %                  tar_center{2} = [0.0 0.5];
                    %                 tar_dimensions{2} = 5*[0.1 0.1]; % width x length
                    %
                    %                     tar_center{3} = [0.0 -0.4];
                    %                     tar_dimensions{3} = [0.1 0.1]; % width x length
                    
                    obj.number_tar = length(tar_center);
                    
                    for i = 1 : obj.number_tar
                        obj.polytopes.tar{i}.P = [eye(2);-eye(2)];
                        obj.polytopes.tar{i}.q = [tar_center{i}(1) + tar_dimensions{1}(1)/2;
                            tar_center{i}(2) + tar_dimensions{i}(2)/2;
                            -(tar_center{i}(1) - tar_dimensions{i}(1)/2);
                            -(tar_center{i}(2) - tar_dimensions{i}(2)/2)];
                    end
                    
                case "VLC_sewer_pipes_B_VSS"
                    
                    % Targets
                    tar_center{1} = [0.6,-0.1];
                    tar_dimensions{1} = [0.15,0.15]; % width x length
                    %
                    %                  tar_center{2} = [0.0 0.5];
                    %                 tar_dimensions{2} = 5*[0.1 0.1]; % width x length
                    %
                    %                     tar_center{3} = [0.0 -0.4];
                    %                     tar_dimensions{3} = [0.1 0.1]; % width x length
                    
                    obj.number_tar = length(tar_center);
                    
                    for i = 1 : obj.number_tar
                        obj.polytopes.tar{i}.P = [eye(2);-eye(2)];
                        obj.polytopes.tar{i}.q = [tar_center{i}(1) + tar_dimensions{1}(1)/2;
                            tar_center{i}(2) + tar_dimensions{i}(2)/2;
                            -(tar_center{i}(1) - tar_dimensions{i}(1)/2);
                            -(tar_center{i}(2) - tar_dimensions{i}(2)/2)];
                    end
                    
                    
                    
            end
            
        end
        
        function fig = Plot(obj,n_figure)
            
            % Colors
            for i = 1 : obj.number_agents; obj.agents{i}.color = 'green'; end % followers
            % obj.agents{1}.color = 'red';
            
            % Plot operational region
            fig = figure(n_figure); hold on;
            field = Polyhedron(obj.polytopes.op_region.P,obj.polytopes.op_region.q);
            field.plot('color','white','alpha',0.2)
            
            % Plot obstacles
            for i=1:obj.number_obs
                plot(Polyhedron(obj.polytopes.obs{i}.P,obj.polytopes.obs{i}.q),'color','black');
            end
            
            % Plot targets
            for i=1:obj.number_tar-1
                plot(Polyhedron(obj.polytopes.tar{i}.P,obj.polytopes.tar{i}.q),'color','blue','alpha',0.2);
            end
            plot(Polyhedron(obj.polytopes.tar{obj.number_tar}.P,obj.polytopes.tar{obj.number_tar}.q),'color','blue','alpha',1);
            
            % Initial positions
            for i = 1 : obj.number_agents
                q = [obj.agents{i}.x0(1);
                    obj.agents{i}.x0(3);
                    -obj.agents{i}.x0(1);
                    -obj.agents{i}.x0(3)]+...
                    [obj.polytopes.agent_body{i}.q(1)/2;
                    obj.polytopes.agent_body{i}.q(2)/2;
                    +obj.polytopes.agent_body{i}.q(3)/2;
                    +obj.polytopes.agent_body{i}.q(4)/2];
                
                plot(obj.agents{i}.x0(1),obj.agents{i}.x0(3),'o')
                plot(Polyhedron(obj.polytopes.agent_body{i}.P,q),'color',obj.agents{i}.color);
            end
            
            xlabel('rx (m)'); ylabel('ry (m)');
            
            %             q = [ -2.25; 0; 2.25; 0] + [obj.polytopes.agent_body{1}.q(1)/2;
            %                     obj.polytopes.agent_body{1}.q(2)/2;
            %                     +obj.polytopes.agent_body{1}.q(3)/2;
            %                     +obj.polytopes.agent_body{1}.q(4)/2];
            %
            %               plot(Polyhedron(obj.polytopes.agent_body{1}.P,q));
            
            % xlim([-5.2 5.2]); ylim([-5.2 5.2]);
            
            
            
            axis equal
            
            
        end
        
        function PlotInMPDGrid(obj,n_figure)
            
            % Colors
            for i = 1 : obj.number_agents; obj.agents{i}.color = 'green'; end % followers
            % obj.agents{1}.color = 'red';
            
            % Plot operational region
            fig = figure(n_figure); hold on;
            field = Polyhedron(obj.polytopes.op_region.P,obj.polytopes.op_region.q);
            field.plot('color','white','alpha',0.2);
            
            x_min = -obj.polytopes.op_region.q(3);
            y_min = -obj.polytopes.op_region.q(4);
            x_max = obj.polytopes.op_region.q(1);
            y_max = obj.polytopes.op_region.q(2);
            
            % Plot obstacles
            for i=1:obj.number_obs
                plot(Polyhedron(obj.polytopes.obs{i}.P,obj.polytopes.obs{i}.q),'color','black');
            end
            
            % Plot targets
            for i=1:obj.number_tar-1
                plot(Polyhedron(obj.polytopes.tar{i}.P,obj.polytopes.tar{i}.q),'color','blue','alpha',0.2);
            end
            plot(Polyhedron(obj.polytopes.tar{obj.number_tar}.P,obj.polytopes.tar{obj.number_tar}.q),'color','blue','alpha',1);
            
            % Initial positions
            for i = 1 : obj.number_agents
                q = [obj.agents{i}.x0(1);
                    obj.agents{i}.x0(3);
                    -obj.agents{i}.x0(1);
                    -obj.agents{i}.x0(3)]+...
                    [obj.polytopes.agent_body{i}.q(1)/2;
                    obj.polytopes.agent_body{i}.q(2)/2;
                    +obj.polytopes.agent_body{i}.q(3)/2;
                    +obj.polytopes.agent_body{i}.q(4)/2];
                
                plot(obj.agents{i}.x0(1),obj.agents{i}.x0(3),'o')
                plot(Polyhedron(obj.polytopes.agent_body{i}.P,q),'color',obj.agents{i}.color);
            end
            
            xlabel('rx (m)'); ylabel('ry (m)');
            xticks([x_min:(x_max-x_min)/obj.grid_size_x:x_max]);
            yticks([y_min:(y_max-y_min)/obj.grid_size_y:y_max]);
            

            axis equal;
            
            
        end
        
        function BuildAgents(obj,type)
            
            switch type
                %% Test
                case "test"
                    % Build field of view and orientation polytopes
                    triangle_height = 1;
                    hat_height = 0.2;
                    cone_angle = pi/4;
                    number_angles = 360/15; % divide in angles 15 degrees apart
                    v_max = 0.9; % maximum linear velocity
                    edge_length = triangle_height/cos(cone_angle);
                    
                    % Horizontal and vertical lines (0,90,180,270)degree
                    obj.polytopes.orient{1}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{1}.q = [v_max; 0; 0; 0];
                    obj.polytopes.orient{7}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{7}.q = [0; v_max; 0; 0];
                    obj.polytopes.orient{13}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{13}.q = [0; 0; v_max; 0];
                    obj.polytopes.orient{19}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{19}.q = [0; 0; 0; v_max];
                    
                    % Orientations taken from 0 to 2pi with step 2pi/n_angles
                    angle = [0:2*pi/number_angles:2*pi];
                    
                    for i = 1 : number_angles
                        
                        % Builds fov cone polytopes
                        vertices{i}(1,:) = [0;0];
                        vertices{i}(2,:) = edge_length.*[cos(angle(i)+cone_angle);sin(angle(i)+cone_angle)];
                        vertices{i}(3,:) = edge_length.*[cos(angle(i)-cone_angle);sin(angle(i)-cone_angle)];
                        vertices{i}(4,:) = (triangle_height+hat_height).*[cos(angle(i));sin(angle(i))];
                        
                        fov_cone_vrep = Polyhedron(vertices{i});
                        fov_cone_hrep = fov_cone_vrep.computeHRep;
                        obj.polytopes.fov_cones{i}.P = fov_cone_hrep.A;
                        obj.polytopes.fov_cones{i}.q = fov_cone_hrep.b;
                        
                        % BUILDS LINE SEGMENT POLYTOPES
                        % Parameters from the line tangent to the velocity line
                        % segment
                        phi = pi-angle(i)-pi/2;
                        a_tan = tan(phi);
                        b_tan = v_max*sin(angle(i)) + a_tan*v_max*cos(angle(i));
                        
                        %TODO: this is manual and only works with 24 angles
                        if i~=1 && i~=7 && i~=13 && i~=19 % skips vert/hor cases
                            
                            % P polyope for ith angle
                            obj.polytopes.orient{i}.P = [-tan(angle(i)), 1;
                                tan(angle(i)), -1;
                                a_tan, 1;
                                -a_tan, -1];
                            
                            % q polytope for ith angle
                            obj.polytopes.orient{i}.q = [0; 0; b_tan; 0];
                        end
                        
                        % Correction for 3 and 4 quadrant angles
                        % TODO: this only works considering 24 angles
                        if i > 13 && i~=19
                            obj.polytopes.orient{i}.P(3:4,:) = -obj.polytopes.orient{i-12}.P(3:4,:);
                            obj.polytopes.orient{i}.q = obj.polytopes.orient{i-12}.q;
                        end
                    end
                    
                    % Plot some cones to test
                    %                 for i=14:14
                    %                 figure(1); hold on;
                    %                 cone = Polyhedron(obj.polytopes.fov_cones{i}.P, obj.polytopes.fov_cones{i}.q);
                    %                 line_seg = Polyhedron(obj.polytopes.orient{i}.P,obj.polytopes.orient{i}.q);
                    %                 cone.plot('color','yellow','alpha',0.2);
                    %                 line_seg.plot('color','blue');
                    %               %  plot([0:0.1:1.5].*cos(angle(i)),[0:0.1:1.5].*sin(angle(i)),'b');
                    %                 plot([-2:1:2],zeros(1,5),'k','LineWidth',2);
                    %                 plot(zeros(1,5),[-2:1:2],'k','LineWidth',2);
                    %                 xlim([-2 2]);ylim([-2 2]);
                    %                 end
                    %
                    
                    % Base parameterrs
                    
                    
                    
                    
                    % Number of agents
                    obj.number_agents = 4;
                    
                    % All agents are equal in this case
                    A = [0 1 0 0; 0 0 0 0;0 0 0 1; 0 0 0 0];
                    obj.nx = size(A,1);
                    B = [0 0; 1 0;0 0;0 1];
                    obj.nu = size(B,2);
                    C = [1 0 0 0;0 0 1 0];
                    obj.ny = size(C,1);
                    D = zeros(obj.ny,obj.nu);
                    Ts = 1; % seconds;
                    agent_body_length = 0.1;
                    agent_body_width = 0.1;
                    
                    % Initial states
                    ini_state =  [-1.5 -0.5 -1.0 -1.0;
                        0.0  0.0  0.0 0.0;
                        -1.0 -1.0 -1.0 -0.75;
                        0.0  0.0  0.0 0.0];
                    
                    
                    for i = 1 : obj.number_agents
                        
                        obj.agents{i}.x0 = ini_state(:,i);
                        
                        % Dynamics
                        obj.agents{i}.dyn = c2d(ss(A,B,C,D),Ts);
                        
                        % Body polytope
                        obj.polytopes.agent_body{i}.P = [eye(2);-eye(2)];
                        obj.polytopes.agent_body{i}.q = [agent_body_length/2;
                            agent_body_width/2;
                            agent_body_length/2;
                            agent_body_width/2];
                    end
                    
                    %% sewer_pipes
                case "vss_sewer_pipes"
                    
                    % Build field of view and orientation polytopes
                    triangle_height = 0.5;
                    hat_height = 0.2;
                    cone_angle = pi/6;
                    number_angles = 360/15; % divide in angles 15 degrees apart
                    v_max = 0.9; % maximum linear velocity
                    edge_length = triangle_height/cos(cone_angle);
                    
                    % Horizontal and vertical lines (0,90,180,270)°
                    obj.polytopes.orient{1}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{1}.q = [v_max; 0; 0; 0];
                    obj.polytopes.orient{7}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{7}.q = [0; v_max; 0; 0];
                    obj.polytopes.orient{13}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{13}.q = [0; 0; v_max; 0];
                    obj.polytopes.orient{19}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{19}.q = [0; 0; 0; v_max];
                    
                    % Orientations taken from 0 to 2pi with step 2pi/n_angles
                    angle = [0:2*pi/number_angles:2*pi];
                    
                    for i = 1 : number_angles
                        
                        % Builds fov cone polytopes
                        vertices{i}(1,:) = [0;0];
                        vertices{i}(2,:) = edge_length.*[cos(angle(i)+cone_angle);sin(angle(i)+cone_angle)];
                        vertices{i}(3,:) = edge_length.*[cos(angle(i)-cone_angle);sin(angle(i)-cone_angle)];
                        vertices{i}(4,:) = (triangle_height+hat_height).*[cos(angle(i));sin(angle(i))];
                        
                        fov_cone_vrep = Polyhedron(vertices{i});
                        fov_cone_hrep = fov_cone_vrep.computeHRep;
                        obj.polytopes.fov_cones{i}.P = fov_cone_hrep.A;
                        obj.polytopes.fov_cones{i}.q = fov_cone_hrep.b;
                        
                        % BUILDS LINE SEGMENT POLYTOPES
                        % Parameters from the line tangent to the velocity line
                        % segment
                        phi = pi-angle(i)-pi/2;
                        a_tan = tan(phi);
                        b_tan = v_max*sin(angle(i)) + a_tan*v_max*cos(angle(i));
                        
                        %TODO: this is manual and only works with 24 angles
                        if i~=1 && i~=7 && i~=13 && i~=19 % skips vert/hor cases
                            
                            % P polyope for ith angle
                            obj.polytopes.orient{i}.P = [-tan(angle(i)), 1;
                                tan(angle(i)), -1;
                                a_tan, 1;
                                -a_tan, -1];
                            
                            % q polytope for ith angle
                            obj.polytopes.orient{i}.q = [0; 0; b_tan; 0];
                        end
                        
                        % Correction for 3 and 4 quadrant angles
                        % TODO: this only works considering 24 angles
                        if i > 13 && i~=19
                            obj.polytopes.orient{i}.P(3:4,:) = -obj.polytopes.orient{i-12}.P(3:4,:);
                            obj.polytopes.orient{i}.q = obj.polytopes.orient{i-12}.q;
                        end
                    end
                    
                    
                    % Number of agents
                    obj.number_agents = 4;
                    
                    % All agents are equal in this case
                    A = [0 1 0 0; 0 0 0 0;0 0 0 1; 0 0 0 0];
                    obj.nx = size(A,1);
                    B = [0 0; 1 0;0 0;0 1];
                    obj.nu = size(B,2);
                    C = [1 0 0 0;0 0 1 0];
                    obj.ny = size(C,1);
                    D = zeros(obj.ny,obj.nu);
                    Ts = 1; % seconds;
                    agent_body_length = 0.05;
                    agent_body_width = 0.05;
                    
                    % Base parameters
                    obj.base.pos = [-0.7;0.5];
                    obj.base.orient_index = 19;
                    
                    
                    % Initial states
                    ini_state =  [ -0.7 -0.7 -0.7 -0.7;
                        0.0  0.0 0.0 0.0;
                        0.4 0.3 0.2 0.1;
                        0.0  0.0 0.0 0.0];
                    
                    
                    for i = 1 : obj.number_agents
                        
                        obj.agents{i}.x0 = ini_state(:,i);
                        
                        % Dynamics
                        obj.agents{i}.dyn = c2d(ss(A,B,C,D),Ts);
                        
                        % Body polytope
                        obj.polytopes.agent_body{i}.P = [eye(2);-eye(2)];
                        obj.polytopes.agent_body{i}.q = [agent_body_length/2;
                            agent_body_width/2;
                            agent_body_length/2;
                            agent_body_width/2];
                    end
                    
                    %% sewer_pipes
                case "VLC_sewer_pipes"
                    % Build field of view and orientation polytopes
                    triangle_height = 1.8;
                    
                    hat_height = 0.4;
                    cone_angle = pi/9;
                    number_angles = 360/15; % divide in angles 15 degrees apart
                    v_max = 0.9; % maximum linear velocity
                    edge_length = triangle_height/cos(cone_angle);
                    
                    % Horizontal and vertical lines (0,90,180,270)°
                    obj.polytopes.orient{1}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{1}.q = [v_max; 0; 0; 0];
                    obj.polytopes.orient{7}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{7}.q = [0; v_max; 0; 0];
                    obj.polytopes.orient{13}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{13}.q = [0; 0; v_max; 0];
                    obj.polytopes.orient{19}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{19}.q = [0; 0; 0; v_max];
                    
                    % Orientations taken from 0 to 2pi with step 2pi/n_angles
                    angle = [0:2*pi/number_angles:2*pi];
                    
                    for i = 1 : number_angles
                        
                        % Builds fov cone polytopes
                        vertices{i}(1,:) = [0;0];
                        vertices{i}(2,:) = edge_length.*[cos(angle(i)+cone_angle);sin(angle(i)+cone_angle)];
                        vertices{i}(3,:) = edge_length.*[cos(angle(i)-cone_angle);sin(angle(i)-cone_angle)];
                        vertices{i}(4,:) = (triangle_height+hat_height).*[cos(angle(i));sin(angle(i))];
                        
                        fov_cone_vrep = Polyhedron(vertices{i});
                        fov_cone_hrep = fov_cone_vrep.computeHRep;
                        obj.polytopes.fov_cones{i}.P = fov_cone_hrep.A;
                        obj.polytopes.fov_cones{i}.q = fov_cone_hrep.b;
                        
                        % BUILDS LINE SEGMENT POLYTOPES
                        % Parameters from the line tangent to the velocity line
                        % segment
                        phi = pi-angle(i)-pi/2;
                        a_tan = tan(phi);
                        b_tan = v_max*sin(angle(i)) + a_tan*v_max*cos(angle(i));
                        
                        %TODO: this is manual and only works with 24 angles
                        if i~=1 && i~=7 && i~=13 && i~=19 % skips vert/hor cases
                            
                            % P polyope for ith angle
                            obj.polytopes.orient{i}.P = [-tan(angle(i)), 1;
                                tan(angle(i)), -1;
                                a_tan, 1;
                                -a_tan, -1];
                            
                            % q polytope for ith angle
                            obj.polytopes.orient{i}.q = [0; 0; b_tan; 0];
                        end
                        
                        % Correction for 3 and 4 quadrant angles
                        % TODO: this only works considering 24 angles
                        if i > 13 && i~=19
                            obj.polytopes.orient{i}.P(3:4,:) = -obj.polytopes.orient{i-12}.P(3:4,:);
                            obj.polytopes.orient{i}.q = obj.polytopes.orient{i-12}.q;
                        end
                    end
                    
                    
                    % Number of agents
                    obj.number_agents = 4;
                    
                    % All agents are equal in this case
                    A = [0 1 0 0; 0 0 0 0;0 0 0 1; 0 0 0 0];
                    obj.nx = size(A,1);
                    B = [0 0; 1 0;0 0;0 1];
                    obj.nu = size(B,2);
                    C = [1 0 0 0;0 0 1 0];
                    obj.ny = size(C,1);
                    D = zeros(obj.ny,obj.nu);
                    Ts = 1; % seconds;
                    agent_body_length = 0.2;
                    agent_body_width = 0.2;
                    
                    % Base parameters
                    obj.base.pos = [-2.5;2.5];
                    obj.base.orient_index = 19;
                    
                    
                    
                    % Initial states
                    ini_state =  [ -2.5 -2.5 -2.5 -2.5;
                        0.0  0.0 0.0 0.0;
                        2.0 1.5 1.0 0.5;
                        0.0  0.0 0.0 0.0];
                    
                    
                    for i = 1 : obj.number_agents
                        
                        obj.agents{i}.x0 = ini_state(:,i);
                        
                        % Dynamics
                        obj.agents{i}.dyn = c2d(ss(A,B,C,D),Ts);
                        
                        % Body polytope
                        obj.polytopes.agent_body{i}.P = [eye(2);-eye(2)];
                        obj.polytopes.agent_body{i}.q = [agent_body_length;
                            agent_body_width;
                            agent_body_length;
                            agent_body_width];
                    end
                    
                case "VLC_sewer_pipes_B"
                    % Build field of view and orientation polytopes
                    triangle_height = 2.2*1;
                    
                    hat_height = 0.2;
                    cone_angle = pi/6;
                    number_angles = 360/15; % divide in angles 15 degrees apart
                    v_max = 0.9; % maximum linear velocity
                    edge_length = triangle_height/cos(cone_angle);
                    
                    % Horizontal and vertical lines (0,90,180,270)°
                    obj.polytopes.orient{1}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{1}.q = [v_max; 0; 0; 0];
                    obj.polytopes.orient{7}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{7}.q = [0; v_max; 0; 0];
                    obj.polytopes.orient{13}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{13}.q = [0; 0; v_max; 0];
                    obj.polytopes.orient{19}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{19}.q = [0; 0; 0; v_max];
                    
                    % Orientations taken from 0 to 2pi with step 2pi/n_angles
                    angle = [0:2*pi/number_angles:2*pi];
                    
                    for i = 1 : number_angles
                        
                        % Builds fov cone polytopes
                        vertices{i}(1,:) = [0;0];
                        vertices{i}(2,:) = edge_length.*[cos(angle(i)+cone_angle);sin(angle(i)+cone_angle)];
                        vertices{i}(3,:) = edge_length.*[cos(angle(i)-cone_angle);sin(angle(i)-cone_angle)];
                        vertices{i}(4,:) = (triangle_height+hat_height).*[cos(angle(i));sin(angle(i))];
                        
                        fov_cone_vrep = Polyhedron(vertices{i});
                        fov_cone_hrep = fov_cone_vrep.computeHRep;
                        obj.polytopes.fov_cones{i}.P = fov_cone_hrep.A;
                        obj.polytopes.fov_cones{i}.q = fov_cone_hrep.b;
                        
                        % BUILDS LINE SEGMENT POLYTOPES
                        % Parameters from the line tangent to the velocity line
                        % segment
                        phi = pi-angle(i)-pi/2;
                        a_tan = tan(phi);
                        b_tan = v_max*sin(angle(i)) + a_tan*v_max*cos(angle(i));
                        
                        %TODO: this is manual and only works with 24 angles
                        if i~=1 && i~=7 && i~=13 && i~=19 % skips vert/hor cases
                            
                            % P polyope for ith angle
                            obj.polytopes.orient{i}.P = [-tan(angle(i)), 1;
                                tan(angle(i)), -1;
                                a_tan, 1;
                                -a_tan, -1];
                            
                            % q polytope for ith angle
                            obj.polytopes.orient{i}.q = [0; 0; b_tan; 0];
                        end
                        
                        % Correction for 3 and 4 quadrant angles
                        % TODO: this only works considering 24 angles
                        if i > 13 && i~=19
                            obj.polytopes.orient{i}.P(3:4,:) = -obj.polytopes.orient{i-12}.P(3:4,:);
                            obj.polytopes.orient{i}.q = obj.polytopes.orient{i-12}.q;
                        end
                    end
                    
                    
                    % Number of agents
                    obj.number_agents = 5;
                    
                    % All agents are equal in this case
                    A = [0 1 0 0; 0 0 0 0;0 0 0 1; 0 0 0 0];
                    obj.nx = size(A,1);
                    B = [0 0; 1 0;0 0;0 1];
                    obj.nu = size(B,2);
                    C = [1 0 0 0;0 0 1 0];
                    obj.ny = size(C,1);
                    D = zeros(obj.ny,obj.nu);
                    Ts = 1; % seconds;
                    agent_body_length = 0.2;
                    agent_body_width = 0.2;
                    
                    % Base parameters
                    obj.base.pos = [-2.0;1.5];
                    obj.base.orient_index = 1;
                    
                    
                    
                    % Initial states
                    ini_state =  [ -1.5 -1.0 -0.5 0.0 0.5;
                        0.0  0.0 0.0 0.0 0.0;
                        1.5 1.5 1.5 1.5 1.5;
                        0.0  0.0 0.0 0.0 0.0];
                    
                    
                    for i = 1 : obj.number_agents
                        
                        obj.agents{i}.x0 = ini_state(:,i);
                        
                        % Dynamics
                        obj.agents{i}.dyn = c2d(ss(A,B,C,D),Ts);
                        
                        % Body polytope
                        obj.polytopes.agent_body{i}.P = [eye(2);-eye(2)];
                        obj.polytopes.agent_body{i}.q = [agent_body_length;
                            agent_body_width;
                            agent_body_length;
                            agent_body_width];
                    end
                    
                case "VLC_sewer_pipes_B_VSS"
                    % Build field of view and orientation polytopes
                    triangle_height = 0.6*1;
                    
                    hat_height = 0.2;
                    cone_angle = pi/6;
                    number_angles = 360/15; % divide in angles 15 degrees apart
                    v_max = 0.9; % maximum linear velocity
                    edge_length = triangle_height/cos(cone_angle);
                    
                    % Horizontal and vertical lines (0,90,180,270)°
                    obj.polytopes.orient{1}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{1}.q = [v_max; 0; 0; 0];
                    obj.polytopes.orient{7}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{7}.q = [0; v_max; 0; 0];
                    obj.polytopes.orient{13}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{13}.q = [0; 0; v_max; 0];
                    obj.polytopes.orient{19}.P = [1 0;0 1;-1 0;0 -1]; obj.polytopes.orient{19}.q = [0; 0; 0; v_max];
                    
                    % Orientations taken from 0 to 2pi with step 2pi/n_angles
                    angle = [0:2*pi/number_angles:2*pi];
                    
                    for i = 1 : number_angles
                        
                        % Builds fov cone polytopes
                        vertices{i}(1,:) = [0;0];
                        vertices{i}(2,:) = edge_length.*[cos(angle(i)+cone_angle);sin(angle(i)+cone_angle)];
                        vertices{i}(3,:) = edge_length.*[cos(angle(i)-cone_angle);sin(angle(i)-cone_angle)];
                        vertices{i}(4,:) = (triangle_height+hat_height).*[cos(angle(i));sin(angle(i))];
                        
                        fov_cone_vrep = Polyhedron(vertices{i});
                        fov_cone_hrep = fov_cone_vrep.computeHRep;
                        obj.polytopes.fov_cones{i}.P = fov_cone_hrep.A;
                        obj.polytopes.fov_cones{i}.q = fov_cone_hrep.b;
                        
                        % BUILDS LINE SEGMENT POLYTOPES
                        % Parameters from the line tangent to the velocity line
                        % segment
                        phi = pi-angle(i)-pi/2;
                        a_tan = tan(phi);
                        b_tan = v_max*sin(angle(i)) + a_tan*v_max*cos(angle(i));
                        
                        %TODO: this is manual and only works with 24 angles
                        if i~=1 && i~=7 && i~=13 && i~=19 % skips vert/hor cases
                            
                            % P polyope for ith angle
                            obj.polytopes.orient{i}.P = [-tan(angle(i)), 1;
                                tan(angle(i)), -1;
                                a_tan, 1;
                                -a_tan, -1];
                            
                            % q polytope for ith angle
                            obj.polytopes.orient{i}.q = [0; 0; b_tan; 0];
                        end
                        
                        % Correction for 3 and 4 quadrant angles
                        % TODO: this only works considering 24 angles
                        if i > 13 && i~=19
                            obj.polytopes.orient{i}.P(3:4,:) = -obj.polytopes.orient{i-12}.P(3:4,:);
                            obj.polytopes.orient{i}.q = obj.polytopes.orient{i-12}.q;
                        end
                    end
                    
                    
                    % Number of agents
                    obj.number_agents = 4;
                    
                    % All agents are equal in this case
                    A = [0 1 0 0; 0 0 0 0;0 0 0 1; 0 0 0 0];
                    obj.nx = size(A,1);
                    B = [0 0; 1 0;0 0;0 1];
                    obj.nu = size(B,2);
                    C = [1 0 0 0;0 0 1 0];
                    obj.ny = size(C,1);
                    D = zeros(obj.ny,obj.nu);
                    Ts = 1; % seconds;
                    agent_body_length = 0.074;
                    agent_body_width = 0.074;
                    
                    % Base parameters
                    obj.base.pos = [-0.6;0.6];
                    obj.base.orient_index = 19;
                    
                    
                    
                    % Initial states
                    %                     ini_state =  [ -0.6 -0.6 -0.6 -0.6 -0.6;
                    %                         0.0  0.0 0.0 0.0 0.0;
                    %                         0.5 0.3 0.1 -0.1 -0.3;
                    %                         0.0  0.0 0.0 0.0 0.0];
                    
                    ini_state =  [  -0.6 -0.6 -0.6 -0.6;
                        0.0 0.0 0.0 0.0;
                        0.3 0.1 -0.1 -0.3;
                        0.0 0.0 0.0 0.0];
                    
                    
                    
                    
                    for i = 1 : obj.number_agents
                        
                        obj.agents{i}.x0 = ini_state(:,i);
                        
                        % Dynamics
                        obj.agents{i}.dyn = c2d(ss(A,B,C,D),Ts);
                        
                        % Body polytope
                        obj.polytopes.agent_body{i}.P = [eye(2);-eye(2)];
                        obj.polytopes.agent_body{i}.q = [agent_body_length;
                            agent_body_width;
                            agent_body_length;
                            agent_body_width];
                    end
            end % end build agents
            
        end
        
    end
    
    
end