clc; clearvars; close all; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This code is only used to export Gurobi optimization model to be used in
% python code
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Initializations
cd scenarios;

%scen = collision_scenario_collab;
scen = environment2;
cd ..;
scenario = scenarioClass(scen);

% Initialize agents
cd parameters;
%par = turtlebot3_params("burguer_double_integrator");
%par = husky_params("husky_double_integrator");
par = generic_robot_params("type_1");
par_expert = human_params("average");
cd ..;

for i = 1 : scenario.number_agents-1; agent(i) = AgentClass(par); end
agent(scenario.number_agents) = AgentClass(par_expert);

% Build polytopes
polytopes = PolytopeClass(agent,scenario);

analysis = AnalysisClass(scenario,polytopes);
analysis.PlotInMPDGrid(1)

%% Build MIP
MIP = MIPClass(agent,polytopes);
MIP.BuildOptVarIndexes();

[constraints, cost, options] = MIP.BuildOptProblemToExport(agent,polytopes);

[model,modelRecov] = export(constraints,cost,options);

gurobi(model)
gurobi_write(model,'MPCModel.mps');


% NOT WORKING BECAUSE GUROBI OVERWRITES NEW LABELS WHEN GUROBI_WRITE IS
% CALLED

% % Writing generates labels for the variables that I can now manipulate
% % to facilitate accessing them in the python code.
% model2 = gurobi_read('MPCModel.mps');
% 
% % Since the opt variable X is declared first, I can take the indices
% % directly from here to extract them from de vector V = [X,U,absU...]
% ind_first_ext_agt_state = MIP.index.x(:,end,end-1);
% ind_last_ext_agt_state = MIP.index.x(:,end,end);
% 
% for z = 1 : length(ind_first_ext_agt_state)
%     model2.varnames{ind_first_ext_agt_state(z)} = 'x_{e,"+ z + "}(0)';
%     model2.varnames{ind_last_ext_agt_state(z)} = 'x_{e,"+ z + "}(end)';
%     
% end
% 
% % Save model again with correct labels 
% gurobi_write(model2,'MPCModel.mps');





