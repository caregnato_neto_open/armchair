function scenario_struct = environment1()


%% Environment

%% Field size
epsilon = 0.00;
scenario_struct.op_region_size_y = 7; % m
scenario_struct.op_region_size_x = 7; % m
scenario_struct.grid_size_y = 7;
scenario_struct.grid_size_x = 7;

scenario_struct.cell_size_y = scenario_struct.op_region_size_y/scenario_struct.grid_size_y;
scenario_struct.cell_size_x = scenario_struct.op_region_size_x/scenario_struct.grid_size_x;

scenario_struct.op_region_center = [0,0];

%% Obstacles
% #1
scenario_struct.obs.dimensions{1} = [scenario_struct.cell_size_x , 2*scenario_struct.cell_size_y]+epsilon; % x-axis and y-axis
scenario_struct.obs.center{1} = [-scenario_struct.op_region_size_x/2+2.5*scenario_struct.cell_size_x , scenario_struct.op_region_size_y/2-scenario_struct.cell_size_y];

% #2
scenario_struct.obs.dimensions{2} = [scenario_struct.cell_size_x , 2*scenario_struct.cell_size_y]+epsilon; % x-axis and y-axis
scenario_struct.obs.center{2} = [-scenario_struct.op_region_size_x/2+3.5*scenario_struct.cell_size_x , -scenario_struct.op_region_size_y/2+scenario_struct.cell_size_y];

% #3
scenario_struct.obs.dimensions{3} = [scenario_struct.cell_size_x , scenario_struct.cell_size_y]+epsilon; % x-axis and y-axis
scenario_struct.obs.center{3} = [-scenario_struct.op_region_size_x/2+0.5*scenario_struct.cell_size_x , -scenario_struct.op_region_size_y/2+1.5*scenario_struct.cell_size_y];
 
% #4
scenario_struct.obs.dimensions{4} = [2*scenario_struct.cell_size_x , scenario_struct.cell_size_y]+epsilon; % x-axis and y-axis
scenario_struct.obs.center{4} = [scenario_struct.op_region_size_x/2-1*scenario_struct.cell_size_x , -scenario_struct.op_region_size_y/2+4.5*scenario_struct.cell_size_y];

scenario_struct.number_obs = length(scenario_struct.obs.center);


%% Mission

% ORDERING IS CLOCKWISE THEN CENTER THEN TERMINAL!

% Targets 
% TARGET A
% #1
scenario_struct.tar.dimensions{1} = [scenario_struct.cell_size_x , scenario_struct.cell_size_y]-epsilon; % x-axis and y-axis
coord = [1.5,5.5];
scenario_struct.tar.center{1} = [-scenario_struct.op_region_size_x/2+coord(1)*scenario_struct.cell_size_x , -scenario_struct.op_region_size_y/2+coord(2)*scenario_struct.cell_size_y];

% #2
scenario_struct.tar.dimensions{2} = [scenario_struct.cell_size_x , scenario_struct.cell_size_y]-epsilon; % x-axis and y-axis
coord = [0.5,3.5];
scenario_struct.tar.center{2} = [-scenario_struct.op_region_size_x/2+coord(1)*scenario_struct.cell_size_x , -scenario_struct.op_region_size_y/2+coord(2)*scenario_struct.cell_size_y];

% TARGET B
% #3
coord = [1.5,2.5];
scenario_struct.tar.dimensions{3} = [scenario_struct.cell_size_x , scenario_struct.cell_size_y]-epsilon; % x-axis and y-axis
scenario_struct.tar.center{3} = [-scenario_struct.op_region_size_x/2+coord(1)*scenario_struct.cell_size_x , -scenario_struct.op_region_size_y/2+coord(2)*scenario_struct.cell_size_y];

% #4
coord = [2.5,4.5];
scenario_struct.tar.dimensions{4} = [scenario_struct.cell_size_x , scenario_struct.cell_size_y]-epsilon; % x-axis and y-axis
scenario_struct.tar.center{4} = [-scenario_struct.op_region_size_x/2+coord(1)*scenario_struct.cell_size_x , -scenario_struct.op_region_size_y/2+coord(2)*scenario_struct.cell_size_y];


% #5 LAST TARGET JUST TO FINISH MISSION - NO REWARD
coord = [5.5,5.5];
scenario_struct.tar.dimensions{5} = [scenario_struct.cell_size_x , scenario_struct.cell_size_y]-epsilon; % x-axis and y-axis
scenario_struct.tar.center{5} = [-scenario_struct.op_region_size_x/2+coord(1)*scenario_struct.cell_size_x , -scenario_struct.op_region_size_y/2+coord(2)*scenario_struct.cell_size_y];


scenario_struct.number_tar = length(scenario_struct.tar.center);

%% Agents
scenario_struct.n_agents = 3;

% rx1 = -scenario_struct.op_region_size_x/2+0.5*scenario_struct.cell_size_x + epsilon; % + 0.01 avoids position = 0.00000000...
% ry1 = scenario_struct.op_region_size_y/2-0.5*scenario_struct.cell_size_y + epsilon;
% 
% rx2 = -scenario_struct.op_region_size_x/2+0.5*scenario_struct.cell_size_x + epsilon;
% ry2 = scenario_struct.op_region_size_y/2-4.5*scenario_struct.cell_size_y + epsilon;
% 
% rx3 = -scenario_struct.op_region_size_x/2+0.5*scenario_struct.cell_size_x + epsilon;
% ry3 = scenario_struct.op_region_size_y/2-2.5*scenario_struct.cell_size_y + epsilon;

coord = [4.5,0.5];
rx1 = -scenario_struct.op_region_size_x/2+coord(1)*scenario_struct.cell_size_x + epsilon; % + 0.01 avoids position = 0.00000000...
ry1 = -scenario_struct.op_region_size_y/2+coord(2)*scenario_struct.cell_size_y + epsilon;

coord = [6.5,0.5];
rx2 = -scenario_struct.op_region_size_x/2+coord(1)*scenario_struct.cell_size_x + epsilon;
ry2 = -scenario_struct.op_region_size_y/2+coord(2)*scenario_struct.cell_size_y + epsilon;

coord = [4.5,2.5];
rx3 = -scenario_struct.op_region_size_x/2+coord(1)*scenario_struct.cell_size_x + epsilon;
ry3 = -scenario_struct.op_region_size_y/2+coord(2)*scenario_struct.cell_size_y + epsilon;


scenario_struct.ini_state = [rx1 rx2 rx3
                             0   0   0
                             ry1 ry2 ry3
                             0   0   0];

% scenario_struct.ini_state = [-scenario_struct.op_region_size_x/2+0.5*scenario_struct.cell_size_x -scenario_struct.op_region_size_x/2+1.5*scenario_struct.cell_size_x;
%                               0  0;
%                               scenario_struct.op_region_size_y/2-1.5*scenario_struct.cell_size_y  scenario_struct.op_region_size_y/2-1.5*scenario_struct.cell_size_y;
%                               0  0 ];


              
end

