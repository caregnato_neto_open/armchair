classdef AgentClass < handle
    %MIPCLASS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
       dyn = [];
       specs = [];
       vars = [];
    end
    
    methods
        
        function obj = AgentClass(pars)
            
            % General specs
            obj.specs.length = pars.length;
            obj.specs.width = pars.width;
            obj.specs.max_lin_vel = pars.max_lin_vel;
            obj.specs.min_lin_vel = pars.min_lin_vel;
            obj.specs.max_acc = pars.max_acc;
            obj.specs.min_acc = pars.min_acc;
            obj.specs.conn_region_radius = pars.conn_region_radius;
                        
            % Continuous state-space
            obj.dyn.A = pars.dyn.A;
            obj.dyn.B = pars.dyn.B;
            obj.dyn.C = pars.dyn.C;
            obj.dyn.D = pars.dyn.D;
            
            % Discrete state-space
            obj.dyn.Ad = pars.dyn.Ad;
            obj.dyn.Bd = pars.dyn.Bd;
            obj.dyn.Cd = pars.dyn.Cd;
            obj.dyn.Dd = pars.dyn.Dd;

        end
        
        %%%%%%
        function InitializeVars(obj,MIP,ini_state)
           obj.vars.x = zeros(length(obj.dyn.Ad),MIP.N+1);
           obj.vars.u = zeros(size(obj.dyn.Bd,2),MIP.N);
           obj.vars.vis_tar = zeros(MIP.number_tar,1);
           
           % Initial state
           for i = 1 : MIP.number_robots; obj.vars.x(:,1) = ini_state; end
           
            
        end
        
        %%%%%%
        function visitedTargets = UpdateTargetVisitation(obj,visitedTargets,poly,k)
            
            for t = 1 : length(poly.tar)
                if ~visitedTargets(t)
                pos = [obj.vars.x(1,k); obj.vars.x(3,k)];
                    if all(poly.tar{t}.P*pos <= poly.tar{t}.q+0.01)
                        visitedTargets(t) =  visitedTargets(t) + 1.0;
                    end
                %visitedTargets(t) =  visitedTargets(t) + all(poly.tar{t}.P*pos <= poly.tar{t}.q+0.01);
                end
                
            end
            
        end
        
        %%%%%%
        function next_state = robot_dynamics(obj,xk,uk)
            
          next_state = obj.dyn.Ad * xk + obj.dyn.Bd * uk;
            
        end
        
        
    end % end methods
end % end class

